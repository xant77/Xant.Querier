﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Xant.Querier.Interface
{
    public interface ICriteria : IExpression
    {
        IFactor Factor { get; }
        RelationalOperator Operator { get; }
        IFactor Value { get; }
        void SetOperator(RelationalOperator operatr);
        void SetCompareTo(IFactor value);
        /*void SetCompareTo(IField filedCompareTo);
        void SetCompareTo(IConstant constant);
        void SetCompareTo(IMacro macro);*/

        bool IsValidation();

    }
}
