﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xant.Querier.Interface
{
    public interface IField : IFactor
    {
        /// <summary>
        /// 字段(属性)名称
        /// </summary>
        string Name { get; }
        /// <summary>
        /// 字段(属性)全路径，例如：p.OrderItems.Qty
        /// </summary>
        string Path { get; }
        /// <summary>
        /// 字段(属性)类型
        /// </summary>
        Type Type { get; }

    }
}
