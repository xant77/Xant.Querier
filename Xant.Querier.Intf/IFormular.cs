﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Xant.Querier.Interface
{
    /// <summary>
    /// 算式因子，表达两个因子之间的运算关系
    /// </summary>
    /// <remarks>
    /// 算式因子中的两个因子可以任何类型的因子，当为算式因子时，构成嵌套，可用于表达复杂的算式(例如：(Items.Qty+Item.Adjust)*Items.UnitPrice)
    /// </remarks>
    public interface IFormular : IFactor
    {
        /// <summary>
        /// 左因子
        /// </summary>
        IFactor Left { get; }
        /// <summary>
        /// 算术运算
        /// </summary>
        MathOperator Operator { get; }
        /// <summary>
        /// 右因子
        /// </summary>
        IFactor Right { get; }

        /*废除，已经支持自动分析多级算式中各子算式的运算优先级
        /// <summary>
        /// 该算式因子是否为独立整体，不可分开(表现在SQL语句中为是否包含在一对圆括号中)
        /// </summary>
        bool Unitaried{ get; }

        /// <summary>
        /// 将此算式因子设定为独立整体<see cref="Unitaried"/>
        /// </summary>
        /// <returns>返回因子本身</returns>
        IFormular Unitary();*/

        /// <summary>
        /// 算式中包含的所有字段(递归查找算式中的算式因子)
        /// </summary>
        ReadOnlyCollection<IField> AllFields();

        /// <summary>
        /// 算式中包含的字段(不会递归查找)
        /// </summary>
        /// <returns>返回包含字段因子数组。
        /// 如果两个因子均非字段因子，则数组中元素个数为0，两个因子均为字段因子则元素个数为2，否则元素个数为1。
        /// </returns>
        IField[] Fields();

        /// <summary>
        /// 此算式因子中是否包含字段因子
        /// </summary>
        /// <param name="recursive">是否递归查找</param>
        /// <returns>包含字段因子返回true，否则返回false</returns>
        bool HasFieldInside(bool recursive);

        /// <summary>
        /// 左因子(子算式)运算符优先级是否比此算式本身的运算符优先级低
        /// </summary>
        bool LeftFactorOperatorIsLower { get; }

        /// <summary>
        /// 右因子(子算式)运算符优先级是否比此算式本身的运算符优先级低
        /// </summary>
        bool RightFactorOperatorIsLower { get; }
    }
}
