﻿namespace Xant.Querier.Interface
{
    /// <summary>
    /// Represents that the implemented classes are query specification compilers.
    /// </summary>
    public interface IQueryCompiler
    {
        /// <summary>
        /// Compiles the specified query specification into another representation.
        /// </summary>
        /// <returns>Another representation of the query specification.</returns>
        object Compile();
    }

    /// <summary>
    /// Represents that the implemented classes are query specification compilers.
    /// </summary>
    /// <typeparam name="T">The type of the compiled representation.</typeparam>
    public interface IQueryCompiler<out T> : IQueryCompiler
    {
        /// <summary>
        /// Compiles the specified query specification into another representation.
        /// </summary>
        /// <returns>Another representation of the query specification.</returns>
        new T Compile();
    }
}
