﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xant.Querier.Interface
{
    public interface IQuery
    {
        /// <summary>
        /// 查询名称(仅用于描述，无业务逻辑)
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// 备注(仅用于描述，无业务逻辑)
        /// </summary>
        string Remark { get; set; }
        /// <summary>
        /// 源数据实体的类型
        /// </summary>
        Type SourceEntityType { get; }

        /// <summary>
        /// 查询条件链的首个条件
        /// </summary>
        IExpression RootExpression { get;set; }

        /// <summary>
        /// 指示如何对查询结果排序
        /// </summary>
        IOrderClause OrderClause { get; set; }

        /// <summary>
        /// 指示如何对数据进行分页
        /// </summary>
        IPagination Pagination { get; set; }

    }
}
