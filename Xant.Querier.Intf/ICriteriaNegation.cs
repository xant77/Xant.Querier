namespace Xant.Querier.Interface
{
    public interface ICriteriaNegation : IExpression
    {
        /// <summary>
        /// 一元逻辑操作符，永远执行"非"操作，相当于SQL语句WHERE部分中的"NOT"
        /// </summary>
        UnaryLogicalOperator Operator { get; }
        /// <summary>
        /// 执行一元逻辑操作的条件主体
        /// </summary>
        IExpression Body { get; }
    }
}