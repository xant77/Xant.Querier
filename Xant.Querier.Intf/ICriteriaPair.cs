﻿namespace Xant.Querier.Interface
{
    public interface ICriteriaPair : IExpression
    {
        /// <summary>
        /// 对条件对中两个条件所执行的逻辑操作
        /// </summary>
        LogicalOperator Operator { get; }
        /// <summary>
        /// 左子条件
        /// </summary>
        IExpression Left { get; }
        /// <summary>
        /// 右子条件
        /// </summary>
        IExpression Right { get; }
        
        /// <summary>
        /// 左子条件运算符优先级是否比此算式本身的运算符优先级低
        /// </summary>
        bool LeftExpressionOperatorIsLower { get; }

        /// <summary>
        /// 右子条件运算符优先级是否比此算式本身的运算符优先级低
        /// </summary>
        bool RightExpressionOperatorIsLower { get; }
    
    }
}