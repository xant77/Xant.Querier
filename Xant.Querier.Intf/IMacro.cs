﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xant.Querier.Interface
{
    public interface IMacro: IFactor
    {
        object Execute();
    }
}
