﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Xant.Querier.Interface
{

    public interface IOrderElement
    {
        IFactor Factor { get; }

        OrderRule Rule { get; set; }

    }

    public interface IOrderClause
    {
        ReadOnlyCollection<IOrderElement> Elements { get; }

        bool HasElements { get; }

        IOrderClause Add(params IOrderElement[] items);

        IOrderClause Remove(IOrderElement item);

        IOrderClause RemoveAt(int index);
    }

    public enum OrderRule
    {
        /// <summary>
        /// 升序
        /// </summary>
        Ascending,

        /// <summary>
        /// 降序
        /// </summary>
        Descending,

    }

}
