﻿namespace Xant.Querier.Interface
{
    public interface IExpression
    {
        //IQuery OwnerQuery { get; }

        ICriteriaPair And(IExpression expression);

        ICriteriaPair Or(IExpression expression);

        ICriteriaNegation Not();

    }
}