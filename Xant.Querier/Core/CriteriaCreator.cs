using System;


namespace Xant.Querier.Core
{
    public static class CriteriaCreator
    {
        public static Criteria Create(Factor factor)
        {
            return new Criteria(factor);
        }

        #region EqualTo
        public static Criteria EqualTo(this Factor factor, Factor value)
        {
            string operatorName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            RelationalOperator operatr = OperatorParser.Parse(operatorName);
            Criteria criteria = new Criteria(factor, operatr, value);
            return criteria;
        }

        /*public static Criteria EqualTo(this Factor factor, Macro macro)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.EqualTo, macro);
            return criteria;
        }

        public static Criteria EqualTo(this Factor factor, Constant constant)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.EqualTo, constant);
            return criteria;
        }*/

        public static Criteria EqualTo(this Factor factor, string str)
        {
            Factor value = null;
            if(!string.IsNullOrEmpty(str) && str.StartsWith("{") && str.EndsWith("}"))
            {
                var macroName = str.Substring(1, str.Length - 2);
                value = MacroStore.Find(macroName);
            }
            if(value==null)
            {
                value = new Constant(str);
            }
            Criteria criteria = new Criteria(factor, RelationalOperator.EqualTo, value);
            return criteria;
        }
        
        public static Criteria EqualTo(this Factor factor, Guid? value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.EqualTo, new Constant(value));
            return criteria;
        }
        
        public static Criteria EqualTo(this Factor factor, bool value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.EqualTo, new Constant(value));
            return criteria;
        }
        
        public static Criteria EqualTo(this Factor factor, int value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.EqualTo, new Constant(value));
            return criteria;
        }
        
        public static Criteria EqualTo(this Factor factor, decimal value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.EqualTo, new Constant(value));
            return criteria;
        }
        public static Criteria EqualTo(this Factor factor, DateTime value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.EqualTo, new Constant(value));
            return criteria;
        }
        #endregion

        #region GreaterThan
        public static Criteria GreaterThan(this Factor factor, Factor value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.GreaterThan, value);
            return criteria;
        }

        public static Criteria GreaterThan(this Factor factor, string str)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.GreaterThan, new Constant(str));
            return criteria;
        }

        public static Criteria GreaterThan(this Factor factor, int value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.GreaterThan, new Constant(value));
            return criteria;
        }

        public static Criteria GreaterThan(this Factor factor, decimal value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.GreaterThan, new Constant(value));
            return criteria;
        }

        public static Criteria GreaterThan(this Factor factor, DateTime value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.GreaterThan, new Constant(value));
            return criteria;
        }

        #endregion

        #region GreaterThanOrEqualTo
        public static Criteria GreaterThanOrEqualTo(this Factor factor, Factor value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.GreaterThanOrEqualTo, value);
            return criteria;
        }

        public static Criteria GreaterThanOrEqualTo(this Factor factor, string str)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.GreaterThanOrEqualTo, new Constant(str));
            return criteria;
        }

        public static Criteria GreaterThanOrEqualTo(this Factor factor, int value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.GreaterThanOrEqualTo, new Constant(value));
            return criteria;
        }
        
        public static Criteria GreaterThanOrEqualTo(this Factor factor, decimal value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.GreaterThanOrEqualTo, new Constant(value));
            return criteria;
        }
        
        public static Criteria GreaterThanOrEqualTo(this Factor factor, DateTime value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.GreaterThanOrEqualTo, new Constant(value));
            return criteria;
        }

        #endregion

        #region LessThan
        public static Criteria LessThan(this Factor factor, Factor value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.LessThan, value);
            return criteria;
        }

        public static Criteria LessThan(this Factor factor, string str)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.LessThan, new Constant(str));
            return criteria;
        }
        
        public static Criteria LessThan(this Factor factor, int value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.LessThan, new Constant(value));
            return criteria;
        }
        
        public static Criteria LessThan(this Factor factor, decimal value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.LessThan, new Constant(value));
            return criteria;
        }
        
        public static Criteria LessThan(this Factor factor, DateTime value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.LessThan, new Constant(value));
            return criteria;
        }

        #endregion

        #region LessThanOrEqualTo
        public static Criteria LessThanOrEqualTo(this Factor factor, Factor value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.LessThanOrEqualTo, value);
            return criteria;
        }

        public static Criteria LessThanOrEqualTo(this Factor factor, string str)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.LessThanOrEqualTo, new Constant(str));
            return criteria;
        }
        
        public static Criteria LessThanOrEqualTo(this Factor factor, int value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.LessThanOrEqualTo, new Constant(value));
            return criteria;
        }
        
        public static Criteria LessThanOrEqualTo(this Factor factor, decimal value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.LessThanOrEqualTo, new Constant(value));
            return criteria;
        }
        
        public static Criteria LessThanOrEqualTo(this Factor factor, DateTime value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.LessThanOrEqualTo, new Constant(value));
            return criteria;
        }

        #endregion

        #region Contains
        public static Criteria Contains(this Factor factor, Factor value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.Contains, value);
            return criteria;
        }

        public static Criteria Contains(this Factor factor, string str)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.Contains, new Constant(str));
            return criteria;
        }

        #endregion

        #region StartsWith
        public static Criteria StartsWith(this Factor factor, Factor value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.StartsWith, value);
            return criteria;
        }

        public static Criteria StartsWith(this Factor factor, string str)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.StartsWith, new Constant(str));
            return criteria;
        }

        #endregion

        #region EndsWith
        public static Criteria EndsWith(this Factor factor, Factor value)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.EndsWith, value);
            return criteria;
        }

        public static Criteria EndsWith(this Factor factor, string str)
        {
            Criteria criteria = new Criteria(factor, RelationalOperator.EndsWith, new Constant(str));
            return criteria;
        }

        #endregion

    }
    public static class OperatorParser
    {
        public static RelationalOperator Parse(string operatorName)
        {
            try
            {
                return (RelationalOperator)Enum.Parse(typeof(RelationalOperator), operatorName, true);
            }
            catch (Exception ex)
            {
                //最好抛出自定义的异常类
                throw new Exception(string.Format("{0} is not a valid operator.", operatorName), ex);
            }
        }

    }

}