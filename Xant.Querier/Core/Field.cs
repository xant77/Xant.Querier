﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xant.Querier.Core
{
    public class Field : Factor
    {
        private string path;

        public Type Type { get; private set; }

        public string Path
        {
            get 
            { return path; }
            private set
            {
                path = value;
                if (value.Contains('.'))
                {
                    var nodes = new List<string>(value.Split(new char[] {'.'}, StringSplitOptions.RemoveEmptyEntries));
                    for (int i = nodes.Count - 1; i >= 0; i--)
                    {
                        if (nodes[i].Contains('(') && nodes[i].Contains(')'))
                            nodes.RemoveAt(i);
                    }
                    path = string.Join(".", nodes.ToArray());
                    Name = nodes.Last();
                }
                else
                {
                    Name = path;
                }
            }
        }

        public string Name { get; private set; }
        public Field(string path, Type type)
        {
            this.Path = path;
            this.Type = type;
        }

        public override string ToString()
        {
            return Path;
        }

        public override int GetHashCode()
        {
            return this.Path.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var compare2 = obj as Field;
            return compare2 !=null && this.path.Equals(compare2.path) && this.Type.Equals(compare2.Type);
        }
    }

    //String, Boolean, Integer, Decimal, DateTime, Guid, Byte, ByteArray, 
}
