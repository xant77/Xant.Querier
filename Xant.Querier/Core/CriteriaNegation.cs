﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Xant.Querier.Core
{
    public class CriteriaNegation: Xpression
    {
        /// <summary>
        /// 一元逻辑操作符，永远执行"非"操作，相当于SQL语句WHERE部分中的"NOT"
        /// </summary>
        public UnaryLogicalOperator Operator
        {
            get { return UnaryLogicalOperator.Not; }
        }

        public Xpression Body { get; private set; }

        public CriteriaNegation(Xpression body)
        {
            if (body == null)
            {
                throw new ArgumentNullException("body", "必须指定执行逻辑非操作的条件主体。");
            }
            this.Body = body;
        }

        public override string ToString()
        {
            return "NOT "+Body.ToString();
        }
    }
}
