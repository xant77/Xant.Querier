﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Xant.Querier.Core
{
    /// <summary>
    /// SQL条件。可以使用{实体类名}这样的占位符，在生成最张的WHERE从句时，将根据实体配置信息进行替换
    /// </summary>
    public class SqlCriteria: Xpression
    {
        public string Sql { get; private set; }
        public SqlCriteria(string sql)
        {
            this.Sql = sql;
        }

    }
}
