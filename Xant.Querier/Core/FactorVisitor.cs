﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Xant.Querier.Core
{
    public class FactorVisitor
    {
        protected Factor Factor { get; private set; }

        public FactorVisitor(Factor factor)
        {
            if(factor==null)
            {
                throw new ArgumentNullException("factor");
            }
            this.Factor = factor;
        }

        public void Visit(Stack<Factor> factorStack)
        {
            this.ProcessItem(factorStack, this.Factor);
        }

        private void ProcessItem(Stack<Factor> factorStack, Factor item)
        {
            factorStack.Push(item);
            if (item is Formular)
            {
                var formular = item as Formular;
                this.ProcessItem(factorStack, formular.Left);
                this.ProcessItem(factorStack, formular.Right);
            }
        }

    }
}
