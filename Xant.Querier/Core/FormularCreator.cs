﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Xant.Querier.Core
{
    public static class FormularCreator
    {

        /*public static Formular Create(Field field)
        {
            return new Formular(field);
        }*/

        public static Formular Plus(this Factor factor, Factor factor2)
        {
            Formular formular = new Formular(factor, MathOperator.Plus, factor2);
            return formular;
        }

        public static Formular Plus(this Factor factor, string value)
        {
            return Plus(factor, new Constant(value));
        }

        public static Formular Plus(this Factor factor, int value)
        {
            return Plus(factor, new Constant(value));
        }

        public static Formular Plus(this Factor factor, decimal value)
        {
            return Plus(factor, new Constant(value));
        }

        /*不能用泛型定义，得为各类型分别定义
        public static Formular Plus<T>(this T value1, T value2)
        {
            return Plus(new Constant(value1), new Constant(value2));
        }

        public static Formular Plus<T>(this T constant, Factor value)
        {
            return Plus(new Constant(constant), value);
        }*/

       public static Formular Minus(this Factor factor, Factor factor2)
        {
            Formular formular = new Formular(factor, MathOperator.Minus, factor2);
            return formular;
        }

        public static Formular Minus(this Factor factor, int value)
        {
            return Minus(factor, new Constant(value));
        }

        public static Formular Minus(this Factor factor, decimal value)
        {
            return Minus(factor, new Constant(value));
        }

        public static Formular Multiply(this Factor factor, Factor factor2)
        {
            Formular formular = new Formular(factor, MathOperator.Multiply, factor2);
            return formular;
        }

        public static Formular Multiply(this Factor factor, int value)
        {
            return Multiply(factor, new Constant(value));
        }

        public static Formular Multiply(this Factor factor, decimal value)
        {
            return Multiply(factor, new Constant(value));
        }

        public static Formular Divide(this Factor factor, Factor factor2)
        {
            Formular formular = new Formular(factor, MathOperator.Divide, factor2);
            return formular;
        }

        public static Formular Divide(this Factor factor, int value)
        {
            return Divide(factor, new Constant(value));
        }

        public static Formular Divide(this Factor factor, decimal value)
        {
            return Divide(factor, new Constant(value));
        }

        public static Formular Power(this Factor factor, int n=2)
        {
            Formular formular = new Formular(factor, MathOperator.Power, new Constant(n));
            return formular;
        }

        public static Formular Count(this Factor factor)
        {
            Formular formular = new Formular(factor, MathOperator.Count, null);
            return formular;
        }

        public static Formular Sum(this Factor factor)
        {
            Formular formular = new Formular(factor, MathOperator.Sum, null);
            return formular;
        }

    }
}
