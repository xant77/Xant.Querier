﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;


namespace Xant.Querier.Core
{

    public class OrderElement
    {

        public Factor Factor { get; private set; }

        public OrderRule Rule { get; set; }

        public OrderElement(Field field, OrderRule rule)
        {
            this.Factor = field;
            this.Rule = rule;
        }

        public OrderElement(Formular formular, OrderRule rule)
        {
            //检查formular中至少应该包含一个Field类型因子
            this.Factor = formular;
            this.Rule = rule;
        }

        public OrderElement(Field field)
            : this(field, OrderRule.Ascending)
        {
        }

        public OrderElement(Formular formular)
            : this(formular, OrderRule.Ascending)
        {
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", Factor, Rule == OrderRule.Ascending ? "ASC" : "DESC");
        }

        public override bool Equals(object obj)
        {
            var compare2 = obj as OrderElement;
            return compare2 !=null && this.Factor.Equals(compare2.Factor) && this.Rule==compare2.Rule;
        }

    }

    /// <summary>
    /// 排序规则
    /// </summary>
    public class OrderClause
    {
        private List<OrderElement> elements;

        public OrderClause()
        {
            this.elements = new List<OrderElement>();
        }

        public ReadOnlyCollection<OrderElement> Elements
        {
            get { return this.elements.AsReadOnly(); }
        }

        public bool HasElements
        {
            get { return this.elements.Count > 0; }
        }

        public OrderClause Add(params OrderElement[] items)
        {
            this.elements.AddRange(items);
            return this;
        }

        public OrderClause Remove(OrderElement item)
        {
            this.elements.Remove(item);
            return this;
        }

        public OrderClause RemoveAt(int index)
        {
            this.elements.RemoveAt(index);
            return this;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var element in this.elements)
            {
                if (sb.Length > 0)
                    sb.Append(", ");
                sb.Append(element.ToString());
            }
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            var compare2 = obj as OrderClause;
            if (compare2 == null || this.elements.Count != compare2.elements.Count)
                return false;
            for (int i = 0; i < this.elements.Count; i++)
            {
                if (!this.elements[i].Factor.Equals(compare2.elements[i].Factor) || this.elements[i].Rule != compare2.elements[i].Rule)
                    return false;
            }
            return true;
        }
    
    }

    public enum OrderRule
    {
        /// <summary>
        /// 升序
        /// </summary>
        Ascending,

        /// <summary>
        /// 降序
        /// </summary>
        Descending,

    }

}
