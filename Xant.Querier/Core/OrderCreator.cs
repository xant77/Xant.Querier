using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Xant.Querier.Core
{
    public static class OrderCreator
    {

        public static OrderElement Ascending(this Field field)
        {
            return new OrderElement(field, OrderRule.Ascending);
        }

        public static OrderElement Descending(this Field field)
        {
            return new OrderElement(field, OrderRule.Descending);
        }

        public static OrderElement Ascending(this Formular formular)
        {
            return new OrderElement(formular, OrderRule.Ascending);
        }

        public static OrderElement Descending(this Formular formular)
        {
            return new OrderElement(formular, OrderRule.Descending);
        }


    }
}
