﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;


namespace Xant.Querier.Core
{
    /// <summary>
    /// 条件表达式抽象类
    /// </summary>
    /// <remarks>为避免与Linq表达式类名相同在引用诸多不便，特去掉首字母E</remarks>
    public abstract class Xpression
    {

        public CriteriaPair And(Xpression expression)
        {
            return new CriteriaPair(this, LogicalOperator.And, expression);
        }

        public CriteriaPair Or(Xpression expression)
        {
            return new CriteriaPair(this, LogicalOperator.Or, expression);
        }

        public CriteriaNegation Not()
        {
            return new CriteriaNegation(this);
        }

    }
}
