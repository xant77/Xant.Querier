﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Xant.Querier.Core
{
    public static class Extensions
    {
        /// <summary>
        /// 从因子对象中提取字段路径(只返回路径前缀，如全路径为"Items.Qty"，则只返回"Items.")
        /// </summary>
        /// <param name="factor">因子</param>
        /// <returns>
        /// 如果为因子未含有字段因子(如常量因子或宏因子)则返回null，
        /// 如果因子为字段或含有字段(如算式因子)，则返回首个字段的路径前缀
        /// </returns>
        public static string ExtractPrefixPath(this Factor factor)
        {
            var path = ExtractFieldPath(factor);
            if (!string.IsNullOrEmpty(path))
            {
                var i = path.LastIndexOf('.');
                if (i > 0)
                {
                    path = path.Remove(i + 1);
                }
                else
                {
                    path = string.Empty;
                }
            }
            return path;
        }

        /// <summary>
        /// 从因子对象中提取字段路径
        /// </summary>
        /// <param name="factor">因子</param>
        /// <returns>
        /// 如果为因子未含有字段因子(如常量因子或宏因子)则返回null，
        /// 如果因子为字段或含有字段(如算式因子)，则返回首个字段的路径
        /// </returns>
        public static string ExtractFieldPath(this Factor factor)
        {
            string path = null;
            if (factor is Field)
            {
                path = (factor as Field).Path;
            }
            else if (factor is Formular)
            {
                var hasAnyField = (factor as Formular).HasFieldInside(false);
                if (hasAnyField)
                    path = (factor as Formular).Fields()[0].Path;
            }
            return path;
        }

        public static void PickField(this Factor factor, List<Field> fields)
        {
            if (fields == null)
            {
                throw new ArgumentNullException("fields");
            }
            if (factor is Field)
            {
                fields.Add(factor as Field);
            }
            else if (factor is Formular)
            {
                fields.AddRange((factor as Formular).AllFields());
            }
        }

        /// <summary>
        /// 指定的算术运算符是否为一元运算符
        /// </summary>
        /// <param name="operatr">算术运算符</param>
        /// <returns>为一元运算符时返回true，否则返回false</returns>
        public static bool IsUnary(this MathOperator operatr)
        {
            return operatr == MathOperator.Count || operatr == MathOperator.Sum;
        }

        public static string GetOperatorString(this MathOperator operatr)
        {
            string s;
            switch (operatr)
            {
                case MathOperator.Plus:
                    s = "+";
                    break;
                case MathOperator.Minus:
                    s = "-";
                    break;
                case MathOperator.Multiply:
                    s = "*";
                    break;
                case MathOperator.Divide:
                    s = "/";
                    break;
                case MathOperator.Count:
                    s = "COUNT()";
                    break;
                case MathOperator.Sum:
                    s = "SUM()";
                    break;
                case MathOperator.Power:
                    s = "POWER()";
                    break;
                default:
                    s = operatr.ToString();
                    break;
            }
            return s;
        }

    }

}
