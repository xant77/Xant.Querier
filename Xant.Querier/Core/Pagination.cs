﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Xant.Querier.Core
{
    /// <summary>
    /// 数据分页
    /// </summary>
    /// <remarks>为简单易用(另一个原因是MySQL和Oracle中分页都不必指定排序字段)，数据分页中不再指定排序字段，直接使用Query.OrderClause属性指定的排序，如果该属性为空则以第一个名称以Id结尾的字段排序</remarks>
    public class Pagination
    {
        /// <summary>
        /// 页面大小(每页记录数)
        /// </summary>
        public int PageSize { get; private set; }

        /// <summary>
        /// 当前获取第几页的数据，从0开始
        /// </summary>
        public int PageIndex { get; private set; }

        /// <summary>
        /// 构造数据分页对象
        /// </summary>
        /// <param name="pageSize">页面大小</param>
        /// <param name="pageIndex">获取第几页的数据(从0开始)</param>
        public Pagination(int pageSize, int pageIndex)
        {
            if (pageSize < 0 || pageIndex < 0)
                throw new ArgumentException("pageSize or pageIndex can not less then zero.");
            this.PageSize = pageSize;
            this.PageIndex = pageIndex;
        }

        public override string ToString()
        {
            return string.Format("size:{0}, index:{1}", PageSize, PageIndex);
        }
    }

}
