﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Xant.Querier.Core
{
    public class Query
    {
        /// <summary>
        /// 查询名称(仅用于描述，无业务逻辑)
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 备注(仅用于描述，无业务逻辑)
        /// </summary>
        public string Remark { get; set; }

        public Type SourceEntityType { get; private set; }

        public Xpression RootExpression { get; set; }

        public OrderClause OrderClause { get; set; }

        public Pagination Pagination { get; set; }

        public Query(Type entityType)
        {
            if (entityType == null)
            {
                throw new ArgumentNullException("entityType");
            }
            this.SourceEntityType = entityType;
        }

        public override bool Equals(object obj)
        {
            var compare2 = obj as Query;
            return compare2 != null && this.RootExpression.Equals(compare2.RootExpression)
                && (this.OrderClause==null && compare2.OrderClause==null || this.OrderClause.Equals(compare2.OrderClause));
        }

    }
}
