﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Xant.Querier.Core
{
    public class Criteria: Xpression
    {
        public Factor Factor { get; private set; }
        public RelationalOperator Operator { get; private set; }
        public Factor Value {get; private set; }

        public Criteria(Factor factor)
        {
            if (factor == null)
            {
                throw new ArgumentNullException("factor", "构造查询条件时，必须指定field参数的值。");
            }
            this.Factor = factor;
        }

        public Criteria(Factor factor, RelationalOperator operatr, Factor value)
            :this(factor)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value", "构造查询条件时，必须指定value参数的值。");
            }
            this.Operator = operatr;
            this.Value = value;
        }

        /*public Criteria(Field field, RelationalOperator operatr, Constant constant)
            :this(field)
        {
            this.Operator = operatr;
            this.Value = constant;
        }

        public Criteria(Field field, RelationalOperator operatr, Field fieldCompareTo)
            : this(field)
        {
            this.Operator = operatr;
            this.Value = fieldCompareTo;
        }

        public Criteria(Field field, RelationalOperator operatr, Macro macro)
            : this(field)
        {
            this.Operator = operatr;
            this.Value = macro;
        }*/

        public void SetOperator(RelationalOperator operatr)
        {
            this.Operator = operatr;
        }

        public void SetCompareTo(Factor value)
        {
            this.Value = value;
        }

        /*public void SetCompareTo(Field filedCompareTo)
        {
            this.Value = filedCompareTo;
        }

        public void SetCompareTo(Constant constant)
        {
            this.Value = constant;
        }

        public void SetCompareTo(Macro macro)
        {
            this.Value = macro;
        }*/

        public bool IsValidation()
        {
            return true;
        }

        public override string ToString()
        {
            return string.Format("{0}.{1}({2})", Factor, Operator, Value);
        }

        public override bool Equals(object obj)
        {
            var compare2 = obj as Criteria;
            return compare2 !=null && this.Factor.Equals(compare2.Factor)
                && this.Operator.Equals(compare2.Operator)
                && this.Value.Equals(compare2.Value);
        }
    }

}
