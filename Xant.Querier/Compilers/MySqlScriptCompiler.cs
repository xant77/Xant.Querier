﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xant.Querier.Core;

namespace Xant.Querier.Compilers
{
    /// <summary>
    /// 专用于MySql数据库平台的脚本编译器
    /// </summary>
    public class MySqlScriptCompiler : SqlWhereClauseCompiler
    {
        public MySqlScriptCompiler(Query query)
            : base(query)
        {
        }

        protected override string PerformCompile()
        {
            var whereClause = base.PerformCompile();
            if (Query.Pagination == null || Query.Pagination.PageSize <= 0)//如非指定分页
            {
                return whereClause;
            }
            //LIMIT m,n 其中m是指记录开始的index(以0为基准)，n是指从第m+1条开始，取n条。另外，MySQL和SQLite的LIMIT语法相同
            whereClause += string.Format(" LIMIT {0}, {1}", Query.Pagination.PageSize * Query.Pagination.PageIndex, Query.Pagination.PageSize);
            return whereClause;
        }

    }

}
