﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xant.Querier.Core;
using LinqExpressions = System.Linq.Expressions;

namespace Xant.Querier.Compilers
{
    public static class Extension
    {
        public static string FullName(this Field field, EntityConfiguration config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }
            return string.Format("{0}.{1}", config.TableAlias, field.Name);
        }

        /// <summary>
        /// 根据算术运算符，获得相应的操作函数名
        /// </summary>
        /// <param name="operatr"></param>
        /// <returns></returns>
        public static string GetMethodName(this MathOperator operatr)
        {
            switch (operatr)
            {
                case MathOperator.Plus:
                    return "op_Addition";
                case MathOperator.Minus:
                    return "op_Subtraction";
                case MathOperator.Multiply:
                    return "op_Multiply";
                case MathOperator.Divide:
                    return "op_Division";
                /*case MathOperator.Count:
                    break;
                case MathOperator.Sum:
                    break;
                case MathOperator.Power:
                    break;*/
                default:
                    return operatr.ToString();
            }
        }

        public static LinqExpressions.Expression ConvertTo(this LinqExpressions.Expression expression, Type change2Type)
        {
            if (change2Type == null)
                throw new ArgumentNullException("change2Type");
            if (expression.Type != change2Type)
                expression = LinqExpressions.Expression.Convert(expression, change2Type);
            return expression;
        }


    }

}
