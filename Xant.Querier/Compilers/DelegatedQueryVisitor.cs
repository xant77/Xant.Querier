﻿using System;
using Xant.Querier.Core;

namespace Xant.Querier.Compilers
{
    

    public class DelegatedQueryVisitor : QueryVisitor
    {
        private readonly Action<Criteria> visitCriteria;

        private readonly Action<CriteriaPair> visitCriteriaPair;

        private readonly Action<CriteriaNegation> visitCriteriaNegation;

        public DelegatedQueryVisitor(
            Query query,
            Action<Criteria> visitCriteria,
            Action<CriteriaPair> visitCriteriaPair,
            Action<CriteriaNegation> visitCriteriaNegation)
            : base(query)
        {
            this.visitCriteria = visitCriteria;
            this.visitCriteriaPair = visitCriteriaPair;
            this.visitCriteriaNegation = visitCriteriaNegation;
        }

        protected override void VisitCriteria(Criteria criteria)
        {
            if (this.visitCriteria!= null)
            {
                this.visitCriteria(criteria);
            }
        }

        protected override void VisitCriteriaPair(CriteriaPair pair)
        {
            if (this.visitCriteriaPair != null)
            {
                this.visitCriteriaPair(pair);
            }
        }

        protected override void VisitCriteriaNegation(CriteriaNegation negation)
        {
            if (this.visitCriteriaNegation != null)
            {
                this.visitCriteriaNegation(negation);
            }
        }
    }
}
