﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xant.Querier.Tester.Model
{
    public class PlanOfDelivery:Entity
    {
        public int PlanId { get; set; }

        public decimal Qty { get; set; }

        public DateTime Date { get; set; }
    }
}
