﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xant.Querier.Tester.Model
{
    public class Product : Entity
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Specs { get; set; }
        public string Unit { get; set; }
        public decimal UnitPrice { get; set; }
    }

}
