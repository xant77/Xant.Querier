﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xant.Querier.Tester.Model
{
    public class Order: Entity
    {
        private readonly List<OrderItem> _items = new List<OrderItem>();

        //public Guid BillId { get; set; }

        //SQLite对Guid类型支持不是太完善，改为用int做为主键
        public int BillId { get; set; }
        public string BillNo { get; set; }
        public Supplier Supplier { get; set; }
        public DateTime BillDate { get; set; }
        public decimal TotalQty { get; set; }
        public decimal TotalPrice { get; set; }
        public string Remark { get; set; }
        public int Vesrion { get; set; }
        public bool Invalid { get; set; }

        public List<OrderItem> Items
        {
            get { return _items; }
        }

        public void AddItem(OrderItem item)
        {
            Items.Add(item);
        }

        public void AddItems(IEnumerable<OrderItem> items)
        {
            foreach (var item in items)
            {
                this.AddItem(item);
            }
        }


        internal void UpdateTotal()
        {
            TotalQty = Items.Sum(p => p.Qty);
            TotalPrice = Items.Sum(p => p.Price);
        }
    }
}
