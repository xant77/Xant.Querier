﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xant.Querier.Tester.Model
{
    public class Address:ValueObject
    {
        public string Country { get; private set; }
        public string State { get; private set; }
        public string Street { get; private set; }

        public Address()
        {
        }

        public Address(string country, string state, string street)
        {
            this.Country = country;
            this.State = state;
            this.Street = street;
        }
    }

}
