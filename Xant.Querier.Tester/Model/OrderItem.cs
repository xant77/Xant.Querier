﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xant.Querier.Tester.Model
{
    public class OrderItem:Entity
    {
        private readonly List<PlanOfDelivery> _plans = new List<PlanOfDelivery>();

        public Guid ItemId { get; set; }
        public decimal Qty { get; set; }
        public string Unit { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Price { get; set; }

        public Product Product { get; set; }

        public List<PlanOfDelivery> Plans
        {
            get { return _plans; }
        }

    }
    
}
