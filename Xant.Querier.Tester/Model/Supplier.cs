﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xant.Querier.Tester.Model
{
    public class Supplier: Entity
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public Name Name { get; set; }

        public Address Address { get; set; }

        public DateTime RegisterTime { get; set; }

        public string Remark { get; set; }

        public bool Invalid { get; set; }
    }
}
