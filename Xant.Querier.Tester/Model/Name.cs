﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xant.Querier.Tester.Model
{
    public class Name:ValueObject
    {
        private string nameCn;
        private string nameShort;
        private string nameEn;

        public Name()
        {
        }

        public Name(string nameCn, string nameEn, string nameShort)
        {
            this.nameCn = nameCn;
            this.nameEn = nameEn;
            this.nameShort = nameShort;
        }

        public string NameCn
        {
            get { return nameCn; }
        }

        public string NameEn
        {
            get { return nameEn; }
        }

        public string NameShort
        {
            get { return nameShort; }
        }

    }
}
