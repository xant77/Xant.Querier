﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xant.Querier.Tester.Model;
using LinqExpressions = System.Linq.Expressions;

namespace Xant.Querier.Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            /*var pe = LinqExpressions.Expression.Parameter(typeof(OrderItem), "item");
            var pe_Order = LinqExpressions.Expression.Parameter(typeof(Order), "p");
            var left = LinqExpressions.Expression.Constant(9m); // LinqExpressions.Expression.PropertyOrField(pe, "Qty");
            var right = LinqExpressions.Expression.Constant(5m);
            var where = LinqExpressions.Expression.GreaterThan(left, right);
            var e_items = LinqExpressions.Expression.PropertyOrField(pe_Order, "Items");
            //var e = LinqExpressions.Expression.Call(typeof(Enumerable), "Any", new Type[] { typeof(OrderItem) }, where);
            //var temp = LinqExpressions.Expression.Convert(LinqExpressions.Expression.PropertyOrField(pe, "Items"), typeof(IEnumerable<OrderItem>));
            var e = LinqExpressions.Expression.Call(typeof(Enumerable), "Any", new Type[] { typeof(OrderItem) }, e_items, where);
            */
            using (var test = new XunitTest())
            {
                test.TestLambdaCompiler1();
                test.TestPagination();
                /*
                test.TestLambdaCompiler2();
                test.TestLambdaCompiler4();
                test.TestLambdaCompiler5();
                test.TestQuery1();
                test.TestQuery2();
                test.TestQuery4();
                test.TestFormular1();
                test.TestFormular2();
                test.TestFormular3();
                test.TestFormular4();
                test.TestSerialize1();
                test.TestSerialize2();
                test.TestSerialize3();*/
            }

            Console.ReadLine();
        }

    }

    public class Constant2
    {
        public object Value { get; protected set; }

        public Constant2(object value)
        {
            this.Value = value;
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public bool Test<T>(T value)
        {
            var type = typeof (T);
            var supportedTypes = new Type[]
            {
                typeof (int), typeof (string), typeof (decimal), typeof (double), typeof (float), typeof (bool),
                typeof (DateTime)
            };
            if (Array.IndexOf(supportedTypes, type) < 0)
            {
                return false;
            }
            return true;
        }
    }

}
