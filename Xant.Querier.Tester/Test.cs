﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Xant.Querier.Compilers;
using Xant.Querier.Core;
using Xant.Querier.Tester.Model;
using Xant.Querier.Utils;

namespace Xant.Querier.Tester
{
    public static class Test
    {
        private static SQLiteHelper dbHelper;
        private static List<Order> orders = new List<Order>();

        static Test()
        {
            InitData();
            InitConfig();
        }

        private static void InitConfig()
        {
            string configXml =
                "<Entity Name=\"Order\" TableName=\"xzcOrder\" TableAlias=\"o\" ><Relationship Property=\"Supplier\" TableName=\"supplier\" TableAlias=\"\" ReferenceField=\"SupplierId\" RelatedToField=\"Id\" /><Relationship Property=\"Items\" TableName=\"Items\" TableAlias=\"d\" ReferenceField=\"BillId\" RelatedToField=\"BillId\"><Relationship Property=\"Product\" TableName=\"Product\" TableAlias=\"p\" ReferenceField=\"ProductId\" RelatedToField=\"Id\" /></Relationship></Entity>";
            EntityConfigurationManager.LoadConfiguration(configXml);
        }

        private static void InitData()
        {
            //helper = new SQLiteHelper("Data Source=:memory:;Version=3;");
            dbHelper = new SQLiteHelper(@"Data Source=D:\xzc\Documents\test.db;Version=3;");
            dbHelper.CreateTable(typeof(Order), "xzcOrder");

            var suppliers = new List<Supplier>
            {
                new Supplier
                {
                    Id = Guid.NewGuid(),
                    Code = "S5X",
                    RegisterTime = DateTime.Now,
                    Name = new Name("供应商一", "Supplier1", "S1"),
                    Remark = "S",
                },
                new Supplier
                {
                    Id = Guid.NewGuid(),
                    Code = "S6X",
                    RegisterTime = DateTime.Now,
                    Name = new Name("供应商二", "Supplier2", "S2"),
                    Remark = "X",
                },
                new Supplier
                {
                    Id = Guid.NewGuid(),
                    Code = "S6X-1",
                    RegisterTime = DateTime.Now,
                    Name = new Name("供应商三", "Supplier3", "S3"),
                    Remark = "Z",
                },
                new Supplier
                {
                    Id = Guid.NewGuid(),
                    Code = "S6X-2",
                    RegisterTime = DateTime.Now,
                    Name = new Name("供应商四", "Supplier4", "S4"),
                    Remark = "Z",
                },

            };
            var products = new List<Product>
            {
                new Product
                {
                    Id = Guid.NewGuid(),
                    Code = "A1",
                    Name = "iphone6",
                    Specs = "16G WHITE",
                    Unit = "部",
                    UnitPrice = 5288m,
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Code = "A2",
                    Name = "iphone6",
                    Specs = "64G GOLD",
                    Unit = "部",
                    UnitPrice = 5988m,
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Code = "M1",
                    Name = "Macbook pro",
                    Specs = "13.3inch 8G RAM",
                    Unit = "台",
                    UnitPrice = 10880,
                },
            };
            var order1 = new Order
            {
                BillId = Guid.NewGuid(),
                BillNo = "PO001",
                BillDate = new DateTime(2013, 4, 5),
                Supplier = suppliers.Find(p => p.Code.Equals("S5X")),
                Remark = "X",
            };
            order1.AddItem(
                new OrderItem
                {
                    ItemId = Guid.NewGuid(),
                    Product = products.Find(p => p.Code.Equals("A1")),
                    Qty = 1,
                    Unit = "部",
                    UnitPrice = 5288,
                    Price = 5288,
                }
                );
            order1.UpdateTotal();
            var order2 = new Order
            {
                BillId = Guid.NewGuid(),
                BillNo = "PO002",
                BillDate = new DateTime(2013, 8, 8),
                Supplier = suppliers.Find(p => p.Code.Equals("S6X")),
                Remark = "X",
            };
            order2.AddItem(
                new OrderItem
                {
                    ItemId = Guid.NewGuid(),
                    Product = products.Find(p => p.Code.Equals("A1")),
                    Qty = 1,
                    Unit = "部",
                    UnitPrice = 5288,
                    Price = 5288,
                }
                );
            order2.AddItem(
                new OrderItem
                {
                    ItemId = Guid.NewGuid(),
                    Product = products.Find(p => p.Code.Equals("A2")),
                    Qty = 1,
                    Unit = "部",
                    UnitPrice = 5988,
                    Price = 5988,
                }
                );
            order2.UpdateTotal();
            var order3 = new Order
            {
                BillId = Guid.NewGuid(),
                BillNo = "PO003",
                BillDate = new DateTime(2014, 5, 6),
                Supplier = suppliers.Find(p => p.Code.Equals("S6X")),
            };
            order3.AddItem(
                new OrderItem
                {
                    ItemId = Guid.NewGuid(),
                    Product = products.Find(p => p.Code.Equals("A1")),
                    Qty = 10,
                    Unit = "部",
                    UnitPrice = 52880,
                    Price = 52880,
                }
                );
            order3.AddItem(
                new OrderItem
                {
                    ItemId = Guid.NewGuid(),
                    Product = products.Find(p => p.Code.Equals("M1")),
                    Qty = 1,
                    Unit = "台",
                    UnitPrice = 10880,
                    Price = 10880,
                }
                );
            order3.UpdateTotal();
            var order4 = new Order
            {
                BillId = Guid.NewGuid(),
                BillNo = "PO004",
                BillDate = new DateTime(2014, 9, 9),
                Supplier = suppliers.Find(p => p.Code.Equals("S6X-2")),
                Remark = "Z",
            };
            order4.AddItem(
                new OrderItem
                {
                    ItemId = Guid.NewGuid(),
                    Product = products.Find(p => p.Code.Equals("M1")),
                    Qty = 2,
                    Unit = "部",
                    UnitPrice = 10880,
                    Price = 21760,
                }
                );
            order4.AddItem(
                new OrderItem
                {
                    ItemId = Guid.NewGuid(),
                    Product = products.Find(p => p.Code.Equals("A2")),
                    Qty = 1,
                    Unit = "部",
                    UnitPrice = 5988,
                    Price = 5988,
                }
                );
            order4.UpdateTotal();
            orders = new List<Order>{order1,order2,order3,order4};

            dbHelper.Write(suppliers.AsEnumerable());
            dbHelper.Write(products.AsEnumerable());
            dbHelper.Write(orders.AsEnumerable(), "xzcOrder");
        }

        public static void TestMethod1()
        {
            var helper = new TypeInfoHelper<Order>();
            //var criteria = helper.GetProperty(p => p.BillNo).EqualTo(new StringConstant("T20140909-001"));
            //Console.WriteLine(criteria.ToString());
            //QuerySerializer.Deserialize<Order>("", Format.Xml);
            //QuerySerializer.Serialize(new Query(typeof(Product)), Format.Xml);

            var query = new Query(typeof (Order));
            query.RootCriteria = helper.GetProperty(p => p.BillNo).Contains(new Constant("S6"))
                .And(
                    helper.GetProperty(p => p.BillDate)
                        .GreaterThanOrEqualTo(new Constant(new DateTime(2014, 1, 1))).Not()).Unitary()
                /*.And(helper.GetProperty(p => p.Supplier.SupplierCode).EqualTo(new StringConstant("SPB")))
                .And(helper.GetProperty(p => p.Items.FirstOrDefault().Product.Unit).EqualTo(new StringConstant("件")))
                .And(
                    helper.GetProperty(p => p.Items.FirstOrDefault().Qty)
                        .LessThanOrEqualTo(helper.GetProperty(p => p.TotalAmount)))*/;

            string configXml =
                "<Entity Name=\"Order\" TableName=\"maDeliveryNote\" TableAlias=\"o\" ><Relationship Property=\"Supplier\" TableName=\"basSupplier\" TableAlias=\"\" ReferenceField=\"SupplierId\" RelatedToField=\"SupplierId\" /><Relationship Property=\"Items\" TableName=\"maDeliveryDetail\" TableAlias=\"d\" ReferenceField=\"BillId\" RelatedToField=\"BillId\"><Relationship Property=\"Product\" TableName=\"basProduct\" TableAlias=\"p\" ReferenceField=\"ProdId\" RelatedToField=\"ProdId\" /></Relationship></Entity>";
            var compiler = new SqlWhereClauseCompiler(query);
            EntityConfigurationManager.LoadConfiguration(configXml);
            compiler.UseParameter = false;
            var str = compiler.Compile();
            Console.WriteLine(str);
        }

        public static void TestMethod2()
        {
            var helper = new TypeInfoHelper<Order>();
            //var criteria = helper.GetProperty(p => p.BillNo).EqualTo(new StringConstant("T20140909-001"));
            //Console.WriteLine(criteria.ToString());
            //QuerySerializer.Deserialize<Order>("", Format.Xml);
            //QuerySerializer.Serialize(new Query(typeof(Product)), Format.Xml);

            var query = new Query(typeof (Order));
            query.RootCriteria = helper.GetProperty(p => p.BillNo).Contains("S6")
                .And(helper.GetProperty(p => p.BillDate).GreaterThanOrEqualTo(new Constant(new DateTime(2014, 1, 1))).Not()).Unitary()
                //.And(helper.GetProperty(p=>p.BillNo).Contains(helper.GetProperty(p=>p.Remark)))
                /*.And(helper.GetProperty(p => p.Supplier.SupplierCode).EqualTo(new StringConstant("SPB")))
                .And(helper.GetProperty(p => p.Items.FirstOrDefault().Product.Unit).EqualTo(new StringConstant("件")))
                .And(
                    helper.GetProperty(p => p.Items.FirstOrDefault().Qty)
                        .LessThanOrEqualTo(helper.GetProperty(p => p.TotalAmount)))*/;

            var orders = new List<Order>();
            orders.Add(new Order{BillId = Guid.NewGuid(), BillNo = "S5X", BillDate = new DateTime(2013,3,3), Remark = "S"});
            orders.Add(new Order{BillId = Guid.NewGuid(), BillNo = "S6X-1", BillDate = new DateTime(2013,3,3), Remark = "X"});
            orders.Add(new Order{BillId = Guid.NewGuid(), BillNo = "S6X-2", BillDate = new DateTime(2014,5,6), Remark = "Z"});
            var compiler = new LambdaExpressionCompiler<Order>(query);
            var expression = compiler.Compile();
            var items = orders.Where(expression.Compile()).ToList();
            Console.WriteLine("Find count:"+items.Count.ToString());
            foreach (Order order in items)
            {
                Console.WriteLine(string.Format("{0} {1}", order.BillNo, order.BillDate));
            }
            var items2 =
                orders.Where(
                    p =>
                        p.BillNo.Contains(p.Remark)
                        && !(p.BillDate >= new DateTime(2014, 1, 1))
                        && p.Items.Any(i=>i.Qty>0)
                        ).ToList();
            Console.WriteLine("Find count:"+items2.Count.ToString());
            foreach (Order order in items2)
            {
                Console.WriteLine(string.Format("{0} {1}", order.BillNo, order.BillDate));
            }
        }

        public static void TestMethod3()
        {
            var helper = new TypeInfoHelper<Order>();

            var findOrder = orders.First();
            var query = new Query(typeof (Order));
            query.RootCriteria = helper.GetProperty(p => p.BillId).EqualTo(findOrder.BillId);
            var compiler = new LambdaExpressionCompiler<Order>(query);
            var expression = compiler.Compile();
            var items = orders.Where(expression.Compile()).ToList();
            Debug.Assert(items.Count==1);
            Debug.Assert(items.First().BillNo.Equals(findOrder.BillNo));

            var sqlCompiler = new SqlWhereClauseCompiler(query);
            var where = sqlCompiler.Compile();
            var sql = "SELECT * FROM xzcOrder AS o WHERE " + where;
            Console.WriteLine(sql);
            var reader = dbHelper.Read(sql, sqlCompiler.ParameterValues);
            Debug.Assert(reader.Read());
            Debug.Assert(findOrder.BillNo.Equals(reader["BillNo"]));

        }

        public static void TestFormular()
        {
            var helper = new TypeInfoHelper<Order>();

            var query = new Query(typeof (Order));
            query.RootCriteria = helper.GetProperty(p => p.TotalQty).Plus(helper.GetProperty(p => p.TotalPrice)).Unitary()
                .Multiply(1.23m).GreaterThan(new Constant(100));

            string configXml =
                "<Entity Name=\"Order\" TableName=\"maDeliveryNote\" TableAlias=\"o\" ><Relationship Property=\"Supplier\" TableName=\"basSupplier\" TableAlias=\"\" ReferenceField=\"SupplierId\" RelatedToField=\"SupplierId\" /><Relationship Property=\"Items\" TableName=\"maDeliveryDetail\" TableAlias=\"d\" ReferenceField=\"BillId\" RelatedToField=\"BillId\"><Relationship Property=\"Product\" TableName=\"basProduct\" TableAlias=\"p\" ReferenceField=\"ProdId\" RelatedToField=\"ProdId\" /></Relationship></Entity>";
            var compiler = new SqlWhereClauseCompiler(query);
            EntityConfigurationManager.LoadConfiguration(configXml);
            compiler.UseParameter = false;
            var str = compiler.Compile();
            Console.WriteLine(str);
            compiler.UseParameter = true;
            var str2 = compiler.Compile();
            Console.WriteLine(str2);
            foreach (var parameterValue in compiler.ParameterValues)
            {
                Console.WriteLine(string.Format("{0}={1}", parameterValue.Key, parameterValue.Value));
            }
        }
            
    }
}
