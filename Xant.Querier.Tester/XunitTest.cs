﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using Xant.Querier.Core;
using Xant.Querier.Compilers;
using Xant.Querier.Utils;
using Xant.Querier.Tester.Model;
using Xunit;
using LinqExpressions = System.Linq.Expressions;

namespace Xant.Querier.Tester
{
    public class XunitTest : IDisposable
    {
        private SQLiteHelper dbHelper;
        private List<Order> orders = new List<Order>();

        public XunitTest()
        {
            InitData();
            InitConfig();
        }

        public void Dispose()
        {
            //为了能在SQLite Expert中检查和浏览数据，不在Dispose()时删除表，改为在初始化数据前删除所有表再执行数据写入
            //dbHelper.RemoveAllTables();
            dbHelper.Dispose();
        }

        #region 环境、数据初始化

        private void InitConfig()
        {
            string configXml =
                "<Entity Name=\"Order\" TableName=\"xzcOrder\" TableAlias=\"o\" ><Relationship Property=\"Supplier\" TableName=\"supplier\" TableAlias=\"\" ReferenceField=\"Id\" RelatedToField=\"SupplierId\" /><Relationship Property=\"Items\" TableName=\"Items\" TableAlias=\"d\" ReferenceField=\"BillId\" RelatedToField=\"BillId\"><Relationship Property=\"Product\" TableName=\"Product\" TableAlias=\"p\" ReferenceField=\"Id\" RelatedToField=\"ProductId\" /></Relationship></Entity>";
            EntityConfigurationManager.LoadConfiguration(configXml);
        }

        private void InitData()
        {
            dbHelper = new SQLiteHelper("Data Source=:memory:;Version=3;");
            //dbHelper = new SQLiteHelper(string.Format("Data Source={0};Version=3;", Path.Combine(Environment.CurrentDirectory, "TestDB.db3")));
            dbHelper.RemoveAllTables();
            dbHelper.CreateTable(typeof (Order), "xzcOrder");

            var suppliers = new List<Supplier>
            {
                new Supplier
                {
                    Id = 1,
                    Code = "S5X",
                    RegisterTime = DateTime.Now,
                    Name = new Name("供应商一", "Supplier1", "S1"),
                    Remark = "S",
                },
                new Supplier
                {
                    Id = 2,
                    Code = "S6X",
                    RegisterTime = DateTime.Now,
                    Name = new Name("供应商二", "Supplier2", "S2"),
                    Remark = "X",
                },
                new Supplier
                {
                    Id = 3,
                    Code = "S6X-1",
                    RegisterTime = DateTime.Now,
                    Name = new Name("供应商三", "Supplier3", "S3"),
                    Remark = "Z",
                },
                new Supplier
                {
                    Id = 4,
                    Code = "S6Z-2",
                    RegisterTime = DateTime.Now,
                    Name = new Name("供应商四", "Supplier4", "S4"),
                    Remark = "Z",
                },

            };
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Code = "A1",
                    Name = "iphone6",
                    Specs = "16G WHITE",
                    Unit = "部",
                    UnitPrice = 5288m,
                },
                new Product
                {
                    Id = 2,
                    Code = "A2",
                    Name = "iphone6",
                    Specs = "64G GOLD",
                    Unit = "部",
                    UnitPrice = 5988m,
                },
                new Product
                {
                    Id = 3,
                    Code = "M1",
                    Name = "Macbook pro",
                    Specs = "13.3inch 8G RAM",
                    Unit = "台",
                    UnitPrice = 10880,
                },
            };
            var order1 = new Order
            {
                BillId = 123,
                BillNo = "PO001",
                BillDate = new DateTime(2013, 4, 5),
                Supplier = suppliers.Find(p => p.Code.Equals("S5X")),
                Remark = "X",
            };
            order1.AddItem(
                new OrderItem
                {
                    ItemId = Guid.NewGuid(),
                    Product = products.Find(p => p.Code.Equals("A1")),
                    Qty = 1,
                    Unit = "部",
                    UnitPrice = 5288,
                    Price = 5288,
                }
                );
            order1.Items[0].Plans.Add(new PlanOfDelivery { PlanId = 1, Qty = 1, Date = DateTime.Today.AddMonths(1) });
            order1.UpdateTotal();
            var order2 = new Order
            {
                BillId = 222,
                BillNo = "PO002",
                BillDate = new DateTime(2013, 8, 8),
                Supplier = suppliers.Find(p => p.Code.Equals("S6X")),
                Remark = "X",
            };
            order2.AddItem(
                new OrderItem
                {
                    ItemId = Guid.NewGuid(),
                    Product = products.Find(p => p.Code.Equals("A1")),
                    Qty = 2,
                    Unit = "部",
                    UnitPrice = 5288,
                    Price = 5288*2,
                }
                );
            order2.AddItem(
                new OrderItem
                {
                    ItemId = Guid.NewGuid(),
                    Product = products.Find(p => p.Code.Equals("A2")),
                    Qty = 3,
                    Unit = "部",
                    UnitPrice = 5988,
                    Price = 5988*3,
                }
                );
            order2.UpdateTotal();
            var order3 = new Order
            {
                BillId = 369,
                BillNo = "PO003",
                BillDate = new DateTime(2014, 5, 6),
                Supplier = suppliers.Find(p => p.Code.Equals("S6X")),
            };
            order3.AddItem(
                new OrderItem
                {
                    ItemId = Guid.NewGuid(),
                    Product = products.Find(p => p.Code.Equals("A1")),
                    Qty = 10,
                    Unit = "部",
                    UnitPrice = 52880,
                    Price = 52880,
                }
                );
            order3.AddItem(
                new OrderItem
                {
                    ItemId = Guid.NewGuid(),
                    Product = products.Find(p => p.Code.Equals("M1")),
                    Qty = 1,
                    Unit = "台",
                    UnitPrice = 10880,
                    Price = 10880,
                }
                );
            order3.Items[0].Plans.Add(new PlanOfDelivery { PlanId = 1, Qty = 10, Date = DateTime.Today.AddDays(15) });
            order3.Items[1].Plans.Add(new PlanOfDelivery { PlanId = 2, Qty = 1, Date = DateTime.Today.AddDays(15) });
            order3.UpdateTotal();
            var order4 = new Order
            {
                BillId = 124543,
                BillNo = "PO004",
                BillDate = new DateTime(2012, 2, 2),
                Supplier = suppliers.Find(p => p.Code.Equals("S6Z-2")),
                Remark = "Z",
            };
            order4.AddItem(
                new OrderItem
                {
                    ItemId = Guid.NewGuid(),
                    Product = products.Find(p => p.Code.Equals("M1")),
                    Qty = 2,
                    Unit = "部",
                    UnitPrice = 10880,
                    Price = 21760,
                }
                );
            order4.AddItem(
                new OrderItem
                {
                    ItemId = Guid.NewGuid(),
                    Product = products.Find(p => p.Code.Equals("A2")),
                    Qty = 6,
                    Unit = "部",
                    UnitPrice = 5988,
                    Price = 5988*6,
                }
                );
            order4.UpdateTotal();
            var order5 = new Order
            {
                BillId = 8686,
                BillNo = "S6789",
                BillDate = new DateTime(2014, 9, 9),
                Supplier = suppliers.Find(p => p.Code.Equals("S6X")),
                Remark = "S",
            };
            order5.AddItem(
                new OrderItem
                {
                    ItemId = Guid.NewGuid(),
                    Product = products.Find(p => p.Code.Equals("M1")),
                    Qty = 2,
                    Unit = "台",
                    UnitPrice = 10880,
                    Price = 21760,
                }
                );
            order5.AddItem(
                new OrderItem
                {
                    ItemId = Guid.NewGuid(),
                    Product = products.Find(p => p.Code.Equals("A2")),
                    Qty = 3,
                    Unit = "部",
                    UnitPrice = 5988,
                    Price = 5988*3,
                }
                );
            order5.UpdateTotal();
            orders = new List<Order> {order1, order2, order3, order4, order5};

            dbHelper.Write(suppliers.AsEnumerable());
            dbHelper.Write(products.AsEnumerable());
            dbHelper.Write(orders.AsEnumerable(), "xzcOrder");
        }

        #endregion

        public void Test()
        {
            IQueryable<Order> custs = orders.AsQueryable();
            //创建一个参数c
            var param =
                 LinqExpressions.Expression.Parameter(typeof(Order), "p");
            var left = LinqExpressions.Expression.Property(param, "TotalQty");
            var right = LinqExpressions.Expression.Constant(5m);
            var filter = LinqExpressions.Expression.GreaterThan(left, right);
            var pred = LinqExpressions.Expression.Lambda(filter, param);
            var expr = LinqExpressions.Expression.Call(typeof(Queryable), "Where",
                new Type[] { typeof(Order) },
                LinqExpressions.Expression.Constant(custs), pred);
            //生成动态查询
            IQueryable<Order> query = orders.AsQueryable()
                .Provider.CreateQuery<Order>(expr);

        }

        [Fact]
        public void TestGetProperty()
        {
            //用比较传统的方式定义"订单编号"及"订单供应商编号"两个字段
            var fieldBillNo = new Field("BillNo", typeof (string));
            var fieldSupplierCode = new Field("Supplier.Code", typeof (string));
            //以Linq表达式直接获取"订单编号"及"订单供应商编号"两个字段
            var helper = new TypeInfoHelper<Order>();
            var linqBillNo = helper.GetProperty(p => p.BillNo);
            var linqSupplierCode = helper.GetProperty(p => p.Supplier.Code);
            //调用xunit单元测试方法验证两种方式定义的字段是否相等
            Assert.Equal(fieldBillNo, linqBillNo);
            Assert.Equal(fieldSupplierCode, linqSupplierCode);
        }

        [Fact]
        public void TestLambdaCompiler1()
        {
            System.Linq.Expressions.Expression<Func<Order, bool>> refExpr = p => /*p.Supplier.Code.StartsWith("S") && p.TotalPrice/p.TotalQty>5500*/ 2*(p.TotalQty+p.TotalPrice)>1;
            System.Linq.Expressions.Expression<Func<Order, bool>> refExpr2 = p => (p.Supplier.Code.Contains("S") || p.BillNo.StartsWith("PO")) && p.TotalPrice>10000;
            var helper = new TypeInfoHelper<Order>();
            var query = new Query(typeof(Order))
            {
                RootExpression = helper.GetProperty(p => p.Supplier.Code).StartsWith("S")
                ,//.And(helper.GetProperty(p => p.TotalQty).Plus(helper.GetProperty(p=>p.TotalPrice)).Divide(2).GreaterThan(1)),//测试自动类型转换，从int到decimal
            };
            query.OrderClause = new OrderClause().Add(helper.GetProperty(p => p.Supplier.Code).Ascending()).Add(helper.GetProperty(p => p.TotalPrice).Divide(helper.GetProperty(p => p.TotalQty)).Descending());
            query.Pagination = new Pagination(2, 1);

            //var re = QueryableExecutor.RunQuery(orders.AsQueryable(), query);

            //定义一个生成Lambda表达式的编译器对象
            var lambdaCompiler = new LambdaExpressionCompiler<Order>(query);
            var list = lambdaCompiler.RunQuery(orders.AsQueryable());
            //将查询对象编译为Linq表达式
            var expression = lambdaCompiler.Compile();
            //对订单集合执行linq查询
            var result = orders.Where(expression.Compile());//.OrderBy(p=>p.BillId).OrderByDescending(p=>p.BillDate).Take(1).Skip(2);
            var firstFound = list.FirstOrDefault();
            //验证查询结果
            Assert.True(firstFound.BillNo.StartsWith("PO003"));
            Assert.True(firstFound.TotalQty>=1);
        }

        private IQueryable<T> IListOrderBy<T>(IEnumerable<T> list, string propertyName)
        {
            var elementType = typeof(T);
            var paramExpr = LinqExpressions.Expression.Parameter(elementType, "p");
            var orderByMember = LinqExpressions.Expression.PropertyOrField(paramExpr, "BillId");
            var orderByExp = LinqExpressions.Expression.Lambda(orderByMember, paramExpr);
            LinqExpressions.Expression sourceExpression = list.AsQueryable().Expression;
            Type sourcePropertyType = elementType.GetProperty("BillId").PropertyType;
            LinqExpressions.Expression lambda = LinqExpressions.Expression.Call(typeof(Queryable), "OrderByDescending",
                new Type[] { elementType, sourcePropertyType },
                sourceExpression, orderByExp);
            return list.AsQueryable().Provider.CreateQuery<T>(lambda);
        }

        [Fact]
        public void TestLambdaCompiler2()
        {
            System.Linq.Expressions.Expression<Func<Order, bool>> refExpr2 = p => (p.Supplier.Code.Contains("S") || p.BillNo.StartsWith("PO")) && p.TotalPrice > 10000;
            var helper = new TypeInfoHelper<Order>();
            var query = new Query(typeof(Order))
            {
                /*RootExpression = helper.GetProperty(p => p.BillNo).Contains("S")
                .Or(helper.GetProperty(p => p.BillNo).StartsWith("PO"))
                .Unitary()
                .And(helper.GetProperty(p => p.TotalPrice).GreaterThan(10000)),*/
                RootExpression = helper.GetProperty(p => p.TotalPrice).GreaterThan(10000).And(
                helper.GetProperty(p => p.BillNo).Contains("S")
                .Or(helper.GetProperty(p => p.BillNo).StartsWith("PO"))
                ),
            };
            //定义一个生成Lambda表达式的编译器对象
            var lambdaCompiler = new LambdaExpressionCompiler<Order>(query);
            //将查询对象编译为Linq表达式
            System.Linq.Expressions.Expression<Func<Order, bool>> refExpr = p => p.Supplier.Code.StartsWith("S") && p.TotalPrice/p.TotalQty>5500;
            var expression = lambdaCompiler.Compile();
            //对订单集合执行linq查询
            var firstFound = orders.Where(expression.Compile()).First();
            //验证查询结果
            //Assert.True(firstFound.TotalPrice/firstFound.TotalQty>5500m);
        }

        [Fact]
        public void TestLambdaCompiler3()
        {
            System.Linq.Expressions.Expression<Func<Order, bool>> refExpr = p => p.Supplier.Code.StartsWith("S6") && p.Items.Any(item => item.Plans.Any(plan=>plan.Qty > 5));
            var helper = new TypeInfoHelper<Order>();
            var query = new Query(typeof(Order))
            {
                RootExpression = helper.GetProperty(p => p.Supplier.Code).StartsWith("S6")
                .And(helper.GetProperty(p => p.Items.First().Plans.First().Qty).GreaterThan(5)),
            };
            //定义一个生成Lambda表达式的编译器对象
            var lambdaCompiler = new LambdaExpressionCompiler<Order>(query);
            //将查询对象编译为Linq表达式
            var expression = lambdaCompiler.Compile();
            //对订单集合执行linq查询
            var firstFound = orders.Where(expression.Compile()).First();
            //验证查询结果
            Assert.Equal(firstFound.BillNo, "PO003");
        }

        [Fact]
        public void TestLambdaCompiler4()
        {
            var helper = new TypeInfoHelper<Order>();
            var query = new Query(typeof(Order))
            {
                RootExpression = helper.GetProperty(p => p.Supplier.Code).StartsWith("A")
                .Or(helper.GetProperty(p => p.Supplier.Code).StartsWith("B"))
                .Or(helper.GetProperty(p => p.Supplier.Code).StartsWith("C"))
                .And(helper.GetProperty(p => p.TotalPrice).GreaterThan(10)),
            };
            //定义一个生成Lambda表达式的编译器对象
            var lambdaCompiler = new LambdaExpressionCompiler<Order>(query);
            //将查询对象编译为Linq表达式
            var expression = lambdaCompiler.Compile();
        }

        [Fact]
        public void TestLambdaCompiler5()
        {
            var helper = new TypeInfoHelper<Order>();
            var query = new Query(typeof(Order))
            {
                RootExpression = helper.GetProperty(p => p.Supplier.Code).StartsWith("A")
                .Or(helper.GetProperty(p => p.Supplier.Code).StartsWith("B"))
                .Or(helper.GetProperty(p => p.Supplier.Code).StartsWith("C").And(helper.GetProperty(p => p.TotalPrice).GreaterThan(10)))
                ,
            };
            //定义一个生成Lambda表达式的编译器对象
            var lambdaCompiler = new LambdaExpressionCompiler<Order>(query);
            //将查询对象编译为Linq表达式
            var expression = lambdaCompiler.Compile();
        }

        /// <summary>
        /// 测试查询订单编号以”PO”打头 且 总订购金额/总订购数量(即平均单价)>=5500的订单
        /// </summary>
        [Fact]
        public void TestQuery1()
        {
            var query = new Query(typeof (Order));
            var helper = new TypeInfoHelper<Order>();
            query.RootExpression = helper.GetProperty(p => p.BillNo).StartsWith("PO") //订单编号以”PO”打头
                .And(
                    helper.GetProperty(p => p.TotalPrice).Divide(helper.GetProperty(p => p.TotalQty))
                        .GreaterThanOrEqualTo(5500));//总订购金额/总订购数量(即平均单价)>=5500

            List<Order> items;
            var sqlCompiler = new SqliteScriptCompiler(query){GenSelectPart = true};
            var sql = sqlCompiler.Compile();
            Console.WriteLine(sql);
            var reader = dbHelper.Read(sql);
            //判断查询结果，命中的订单编号依次应为：PO002,PO003,S6789
            Assert.Equal(reader.Read(), true);
            Assert.Equal(reader.GetString(1), "PO002");
            Assert.Equal(reader.Read(), true);
            Assert.Equal(reader.GetString(1), "PO003");
            Assert.Equal(reader.Read(), true);
            Assert.Equal(reader.GetString(1), "PO004");
            Assert.Equal(reader.Read(), false);

            //定义一个生成Lambda表达式的编译器对象
            var lambdaCompiler = new LambdaExpressionCompiler<Order>(query);
            //将查询对象编译为Linq表达式
            System.Linq.Expressions.Expression<Func<Order, bool>> refExpr = p => p.Supplier.Code.StartsWith("S") && p.TotalPrice/p.TotalQty>5500;
            var expression = lambdaCompiler.Compile();
            //对订单集合执行linq查询
            var items2 = orders.Where(expression.Compile()).ToList();
            //验证查询结果中仅包含一张订单，且订单编号与参照订单的编号相同
            Assert.Equal(items2.Count, 3);
            Assert.Equal(items2.First().BillNo, "PO002");
        }

        [Fact]
        public void TestQuery2()
        {
            var helper = new TypeInfoHelper<Order>();
            var query = new Query(typeof(Order))
            {
                RootExpression = helper.GetProperty(p => p.BillNo).StartsWith("PO") //单号以PO打头
                    .Or(helper.GetProperty(p => p.BillDate).GreaterThanOrEqualTo(new DateTime(2014, 1, 1)).Not())//订单日期 不 大于等于 2014-1-1
                    //.And(helper.GetProperty(p=>p.Supplier.Code).Contains(helper.GetProperty(p=>p.Remark)))//供应商编号中包含订单备注内容
                    //测试发现上一条件生成的代码在SQL SERVER中是可以正常的，但是在SQLITE中始终查不出内容，改为固定值查找
                    .And(helper.GetProperty(p => p.Supplier.Code).Contains("X"))//供应商编码中包含字符"X"
                    .And(helper.GetProperty(p => p.Invalid).EqualTo(false)) //订单失效标志为否
                    .And(helper.GetProperty(p => p.Items.FirstOrDefault().Product.Unit).EqualTo("部")) //订购产品的计量单位为"部"
                    .And(
                        //订单明细中各项订购数量小于订单总订购数量，仅为测试，无实际意义
                        helper.GetProperty(p => p.Items.FirstOrDefault().Qty)
                            .LessThan(helper.GetProperty(p => p.TotalQty)))
            };

            List<Order> items;
            var sqlCompiler = new SqliteScriptCompiler(query) { GenSelectPart = true };
            var sql = sqlCompiler.Compile();
            Console.WriteLine(sql);
            var reader = dbHelper.Read(sql);
            //判断查询结果，命中的订单编号依次应为：PO002,PO003,S6789
            Assert.Equal(reader.Read(), true);
            Assert.Equal(reader.GetString(1), "PO002");
            Assert.Equal(reader.Read(), true);
            Assert.Equal(reader.GetString(1), "PO003");
            Assert.Equal(reader.Read(), true);
            Assert.Equal(reader.GetString(1), "S6789");
            Assert.Equal(reader.Read(), false);

            /*var compiler = new LambdaExpressionCompiler<Order>(query);
            var expression = compiler.Compile();
            items = orders.Where(expression.Compile()).ToList();
            Console.WriteLine("Find count:"+items.Count.ToString());
            foreach (Order order in items)
            {
                Console.WriteLine(string.Format("{0} {1}", order.BillNo, order.BillDate));
            }
            var items2 =
                orders.Where(
                    p =>
                        p.BillNo.Contains(p.Remark)
                        && !(p.BillDate >= new DateTime(2014, 1, 1))
                        && p.Items.Any(i=>i.Qty>0)
                        ).ToList();
            Console.WriteLine("Find count:"+items2.Count.ToString());
            foreach (Order order in items2)
            {
                Console.WriteLine(string.Format("{0} {1}", order.BillNo, order.BillDate));
            }*/
        }

        [Fact]
        public void TestQuery3()
        {
            var helper = new TypeInfoHelper<Order>();
            //获得订单集合中的第一张订单，用于之后的单元测试验证
            var findOrder = orders.First();
            //定义一个查询，条件为订单主键等于前面获取的订单对象的主键值
            var query = new Query(typeof(Order))
            {
                RootExpression = helper.GetProperty(p => p.BillId).EqualTo(findOrder.BillId)
            };
            /*第一种编译方式*/
            //定义一个生成Lambda表达式的编译器对象
            var compiler = new LambdaExpressionCompiler<Order>(query);
            //将查询对象编译为Linq表达式
            var expression = compiler.Compile();
            //对订单集合执行linq查询
            var items = orders.Where(expression.Compile()).ToList();
            //验证查询结果中仅包含一张订单，且订单编号与参照订单的编号相同
            Assert.Equal(items.Count, 1);
            Assert.Equal(items.First().BillNo, findOrder.BillNo);

            /*第二种编译方式*/
            //定义一个生成SQL脚本的编译器对象
            var sqlCompiler = new SqliteScriptCompiler(query) { GenSelectPart = true };
            //将查询对象编译为SQL脚本
            var sql = sqlCompiler.Compile();
            Console.WriteLine(sql);
            //执行sql脚本
            var reader = dbHelper.Read(sql, sqlCompiler.ParameterValues);
            //验证返回的结果中只有一条记录，且订单编号与参照订单的编号相同
            Assert.Equal(reader.Read(), true);
            Assert.Equal(findOrder.BillNo, reader["BillNo"]);
            Assert.Equal(reader.Read(), false);//验证只能执行读取一次操作
        }

        [Fact]
        public void TestQuery4()
        {
            var helper = new TypeInfoHelper<Order>();
            var query = new Query(typeof(Order))
            {
                RootExpression = helper.GetProperty(p => p.BillNo).StartsWith("PO") //订单编号以”PO”打头
                    .Or(helper.GetProperty(p => p.BillNo).Contains("X")),
            };

            var sqlCompiler = new SqliteScriptCompiler(query) { GenSelectPart = true, UseParameter= true };
            var sql = sqlCompiler.Compile();
            var pvs = sqlCompiler.ParameterValues.ToArray();
            Assert.Equal(sql, "SELECT * FROM xzcOrder AS o WHERE o.BillNo LIKE @BillNo+'%' OR o.BillNo LIKE '%'+@BillNo2+'%'");
            Assert.Equal(pvs.Length, 2);
            Assert.Equal(pvs[0].Key, "@BillNo");
            Assert.Equal(pvs[0].Value, "PO");
            Assert.Equal(pvs[1].Value, "X");
        }

        [Fact]
        public void TestQuery5()
        {
            var helper = new TypeInfoHelper<Order>();
            var query = new Query(typeof(Order))
            {
                RootExpression = helper.GetProperty(p => p.BillNo).StartsWith("A")
                    .Or(helper.GetProperty(p => p.BillNo).StartsWith("B").And(helper.GetProperty(p => p.BillNo).StartsWith("C")))
                    
                    .Or(helper.GetProperty(p => p.BillNo).StartsWith("D")),
            };

            var sqlCompiler = new SqliteScriptCompiler(query) { GenSelectPart = true};
            var sql = sqlCompiler.Compile();
        }

        [Fact]
        public void TestFormular1()
        {
            var helper = new TypeInfoHelper<Order>();
            //定义一个求平均单价的算式：总金额(TotalPrice)/总数量(TotalQty)
            var formular=helper.GetProperty(p => p.TotalPrice)//总金额
                .Divide(//除以
                helper.GetProperty(p => p.TotalQty)//总数量
                );
            //用xunit单元测试来验证算式的左因子是否为总金额
            Assert.Equal((formular.Left as Field).Name, "TotalPrice");
            //验证运算符是否为"除以"
            Assert.Equal(formular.Operator, MathOperator.Divide);
            //验证右因子是否为总数量
            Assert.Equal((formular.Right as Field).Name, "TotalQty");
        }

        [Fact]
        public void TestFormular2()
        {
            var helper = new TypeInfoHelper<Order>();
            //定义一个算式(仅供演示，无实际意义)：(TotalQty+TotalPrice)/2
            var formular = helper.GetProperty(p => p.TotalQty)//获取TotalQty
                .Plus(//加法运算
                helper.GetProperty(p => p.TotalPrice)//获取TotalPrice
                )
                .Divide(2);
            //定义一个查询对象，条件为上面定义的算式运算结果大于10000
            var query = new Query(typeof (Order))
            {
                RootExpression = formular.GreaterThan(10000),
            };
            var where = new SqliteScriptCompiler(query).Compile();
            //验证生成的where从句是否为：(o.TotalQty+o.TotalPrice) > 10000
            Assert.Equal(where, "(o.TotalQty+o.TotalPrice)/2 > 10000");
        }

        [Fact]
        public void TestFormular3()
        {
            var helper = new TypeInfoHelper<Order>();

            var query = new Query(typeof (Order));
            //条件：(TotalQty+TotalPrice)*1.23>100
            query.RootExpression = helper.GetProperty(p => p.TotalQty)
                .Plus(helper.GetProperty(p => p.TotalPrice))
                .Multiply(1.23m).GreaterThan(100);

            var compiler = new SqliteScriptCompiler(query);
            compiler.UseParameter = false;
            var str = compiler.Compile();
            Console.WriteLine(str);
            Assert.Equal(str, "(o.TotalQty+o.TotalPrice)*1.23 > 100", new StringIgnoreCaseComparer());

            compiler.UseParameter = true;
            var str2 = compiler.Compile();
            Console.WriteLine(str2);
            Assert.Equal(str2, "(o.totalQty+o.totalPrice)*@p1 > @p2", new StringIgnoreCaseComparer());
            Assert.Equal(compiler.ParameterValues.Count(), 2);
            foreach (var parameterValue in compiler.ParameterValues)
            {
                Console.WriteLine(string.Format("{0}={1}", parameterValue.Key, parameterValue.Value));
            }
        }

        [Fact]
        public void TestFormular4()
        {
            //9+8-7*6/5.0+4
            var f1 = new Constant(9).Plus(8).Minus(new Constant(7).Multiply(6).Divide(5m)).Plus(4);
            Assert.Equal(f1.ToString(), "9+8-7*6/5.0+4");
            //(9+8-7)*6/5.0+4
            var f2 = new Constant(9).Plus(8).Minus(7).Multiply(6).Divide(5m).Plus(4);
            Assert.Equal(f2.ToString(), "(9+8-7)*6/5.0+4");
            //(9+8-7)*6/(5.0+4)
            var f3 = new Constant(9).Plus(8).Minus(7).Multiply(6).Divide(new Constant(5m).Plus(4));
            Assert.Equal(f3.ToString(), "(9+8-7)*6/(5.0+4)");
            //9+(8-7)*6/5.0+4
            var f4 = new Constant(9).Plus(new Constant(8).Minus(7).Multiply(6).Divide(5m)).Plus(4);
            Assert.Equal(f4.ToString(), "9+(8-7)*6/5.0+4");
            //9+(8-7)*6/(5.0+4)
            var f5 = new Constant(9).Plus(new Constant(8).Minus(7).Multiply(6).Divide(new Constant(5.4321).Plus(4)));
            Assert.Equal(f5.ToString(), "9+(8-7)*6/(5.4321+4)");
        }

        [Fact]
        public void TestSerialize1()
        {
            var helper = new TypeInfoHelper<Order>();
            //定义一个查询对象，条件为：订单编号 等于 "PO002"
            var query = new Query(typeof(Order)) { RootExpression = helper.GetProperty(p => p.BillNo).EqualTo("PO002") };
            query.OrderClause = new OrderClause().Add(helper.GetProperty(p => p.BillDate).Descending()).Add(helper.GetProperty(p => p.BillId).Ascending());
            query.Pagination = new Pagination(20, 2);

            var compiler = new SqliteScriptCompiler(query);
            var sql = compiler.Compile();
            //将上面定义的查询对象序列化为XML格式的字符串
            var xml = QuerySerializer.Serialize(query, Format.Xml);
            Console.WriteLine(xml);
            //从XML字符串反序列化为查询对象query2
            var query2 = QuerySerializer.Deserialize(xml);
            //验证反序列化后的查询对象与原始查询对象是相等的
            Assert.Equal(query, query2);
        }

        [Fact]
        public void TestSerialize2()
        {
            var helper = new TypeInfoHelper<Order>();
            var query = new Query(typeof (Order));
            //条件：(TotalQty+TotalPrice)*1.23>100
            query.RootExpression = helper.GetProperty(p => p.TotalQty)
                .Plus(helper.GetProperty(p => p.TotalPrice))
                .Multiply(1.23m).GreaterThan(100);
            var xml = QuerySerializer.Serialize(query, Format.Xml);
            Console.WriteLine(xml);
        }

        [Fact]
        public void TestSerialize3()
        {
            var query = new Query(typeof(Order)){Name = "TestQuery",Remark = "测试于"+DateTime.Now.ToString()};
            var helper = new TypeInfoHelper<Order>();
            query.RootExpression = helper.GetProperty(p => p.BillNo).StartsWith("PO") //单号以PO打头
                .Or(helper.GetProperty(p => p.BillDate).GreaterThanOrEqualTo(new DateTime(2014, 1, 1)).Not())
                //订单日期 不 大于等于 2014-1-1
                //.And(helper.GetProperty(p=>p.Supplier.Code).Contains(helper.GetProperty(p=>p.Remark)))//供应商编号中包含订单备注内容
                .And(helper.GetProperty(p => p.Supplier.Code).Contains("X"))
                //测试发现上一条件生成的代码在SQL SERVER中是可以正常的，但是在SQLITE中始终查不出内容，改为固定值查找
                .And(helper.GetProperty(p => p.Invalid).EqualTo(false)) //订单失效标志为否
                .And(helper.GetProperty(p => p.Items.FirstOrDefault().Product.Unit).EqualTo("部")) //订购产品的计量单位为"部"
                .And(
                    helper.GetProperty(p => p.Items.FirstOrDefault().Qty) //订单明细中各项订购数量小于订单总订购数量，仅为测试，无实际意义
                        .LessThan(helper.GetProperty(p => p.TotalQty)));
            var xml = QuerySerializer.Serialize(query, Format.Xml);
            Console.WriteLine(xml);
            File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "test3.xml"), xml);
            var query2 = QuerySerializer.Deserialize(xml);
            var xml2 = QuerySerializer.Serialize(query2, Format.Xml);
            Assert.Equal(xml, xml2);
        }

        [Fact]
        public void TestPagination()
        {
            var helper = new TypeInfoHelper<Order>();
            var query = new Query(typeof(Order))
            {
                Name = "TestQuery",
                Remark = "测试于" + DateTime.Now.ToString(),
                Pagination = new Pagination(2,1),
            };
            var compiler1 = new MsSqlScriptCompiler(query) { GenSelectPart = true };
            var sql = compiler1.Compile();
            Console.WriteLine(sql);
            var compiler2 = new SqliteScriptCompiler(query) { GenSelectPart = true };
            var sql2 = compiler2.Compile();
            var reader = dbHelper.Read(sql2);
            while (reader.Read())
            {
                Console.WriteLine(string.Format("{0} {1}", reader.GetValue(0), reader.GetString(1)));
            }
        }

    }

    class StringIgnoreCaseComparer : IEqualityComparer<string>
    {
        public bool Equals(string x, string y)
        {
            if (x == null)
                return y == null;
            else
                return x.Equals(y, StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(string str)
        {
            return str.GetHashCode();
        }
    }
}
